# code_gen

This is a prototype of an idea I've had to "generate code" that essentially acts as an executable set of instructions which can then be run on inputs and produce outputs, similar to Machine Learning in a way. It's by no means in a completely useful or fleshed out state, but it is functional and interesting.
