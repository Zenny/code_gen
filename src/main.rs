use crate::codes::{Block, Main, Var, Compare, Comparison, Connector, If, Loop};
use rand;
use rand::Rng;
use std::collections::HashMap;
use std::rc::Rc;
use std::sync::Mutex;
use crate::wraptype::{VarOp, Type};
use rand::prelude::IteratorRandom;
use strum::IntoEnumIterator;
use rand::prelude::SliceRandom;
use rand::rngs::ThreadRng;
use crate::codes::RunDisplay;
use std::ops::RangeInclusive;

mod codes;
mod wraptype;
#[cfg(test)]
mod tests;

const INT_RANGE: RangeInclusive<i128> = -10000..=10000;
const FLOAT_RANGE: RangeInclusive<f64> = -10000.0..=10000.0;

fn find_scope_matches(
    ty: Type,
    keys: &Vec<u16>,
    reference: Rc<Mutex<HashMap<u16, Type>>>,
    vars: Rc<Mutex<HashMap<u16, Type>>>,
    opts: &mut Vec<Type>,
) {
    let lock = reference.lock().unwrap();
    for key in keys {
        if ty.matches(lock.get(key).unwrap().clone()) {
            opts.push(Type::Scope(*key, vars.clone()));
        }
    }
}

fn gen_type(r: &mut ThreadRng, vars: Rc<Mutex<HashMap<u16, Type>>>, keys: &mut Vec<u16>) -> Type {
    match r.gen_range(0..Type::iter().count() - if keys.len() == 0 { 2 } else { 1 }) {
        0 => Type::Float(r.gen_range(FLOAT_RANGE)),
        1 => Type::Int(r.gen_range(INT_RANGE)),
        2 => Type::Bool(r.gen()),
        3 => Type::Scope(keys.choose(r).unwrap().clone(), vars),
        _ => unreachable!()
    }
}

fn gen_forced_type(r: &mut ThreadRng,
                   ty: Type,
                   vars: Rc<Mutex<HashMap<u16, Type>>>,
                   keys: &mut Vec<u16>,
                   reference: Rc<Mutex<HashMap<u16, Type>>>
) -> Type {
    match ty {
        Type::Float(_) => {
            match r.gen_range(0..2 - if keys.len() == 0 { 1 } else { 0 }) {
                0 => Type::Float(r.gen_range(FLOAT_RANGE)),
                1 => {
                    let mut opts = vec![];
                    find_scope_matches(ty, keys, reference, vars, &mut opts);
                    opts.choose(r).unwrap_or(&Type::Float(r.gen_range(FLOAT_RANGE))).clone()
                },
                _ => unreachable!()
            }
        }
        Type::Int(_) => {
            match r.gen_range(0..2 - if keys.len() == 0 { 1 } else { 0 }) {
                0 => Type::Int(r.gen_range(INT_RANGE)),
                1 => {
                    let mut opts = vec![];
                    find_scope_matches(ty, keys, reference, vars, &mut opts);
                    opts.choose(r).unwrap_or(&Type::Int(r.gen_range(INT_RANGE))).clone()
                },
                _ => unreachable!()
            }
        }
        Type::Bool(_) => {
            match r.gen_range(0..2 - if keys.len() == 0 { 1 } else { 0 }) {
                0 => Type::Bool(r.gen()),
                1 => {
                    let mut opts = vec![];
                    find_scope_matches(ty, keys, reference, vars, &mut opts);
                    opts.choose(r).unwrap_or(&Type::Bool(r.gen())).clone()
                },
                _ => unreachable!()
            }
        }
        Type::Scope(a, _) => {
            let lock = reference.lock().unwrap();
            let ref_ty = lock.get(&a).unwrap().clone();
            let mut opts = vec![
                match ref_ty {
                    Type::Float(_) => Type::Float(r.gen_range(FLOAT_RANGE)),
                    Type::Int(_) => Type::Int(r.gen_range(INT_RANGE)),
                    Type::Bool(_) => Type::Bool(r.gen()),
                    _ => unreachable!(),
                }
            ];

            for key in keys {
                if *key == a {
                    continue;
                }
                if ref_ty.matches(lock.get(key).unwrap().clone()) {
                    opts.push(Type::Scope(*key, vars.clone()));
                }
            }

            opts.choose(r).unwrap().clone()
        }
        _ => unreachable!()
    }
}

#[derive(Clone, Copy)]
enum Outer {
    If,
    For,
    Block,
}

impl Outer {
    pub fn is_for(&self) -> bool {
        if let Outer::For = self {
            true
        } else {
            false
        }
    }
}

fn gen_runnable(
    r: &mut ThreadRng,
    reference: Rc<Mutex<HashMap<u16, Type>>>,
    vars: Rc<Mutex<HashMap<u16, Type>>>,
    keys: &mut Vec<u16>,
    key: &mut u16,
    outer: Outer,
    outer_parent: Outer,
    recur: u16,
) -> Option<Box<dyn RunDisplay>> {
    macro_rules! block {
        () => {
            let mut our_keys = keys.clone();
            let mut block = Vec::new();
            let mut go = true;
            let mut added: u32 = 0;
            while go {
                if let Some(runnable) = gen_runnable(r, reference.clone(), vars.clone(), &mut our_keys, key, Outer::Block, outer, recur+1) {
                    block.push(runnable);
                    let p = r.gen();
                    go = r.gen_bool(p);
                    added += 1;
                } else {
                    go = false;
                }
            }
            if added == 0 {
                return None;
            }
            return Some(Box::new(Block(block)));
        }
    }
    if recur > 30 {
        return None;
    }
    match r.gen_range(0..=(3 - if let Outer::Block = outer { 1 } else { 0 })) {
        0 => {
            // var
            let mut ty = gen_type(r, vars.clone(), keys);

            let mut opts = vec![];
            for key in keys.iter() {
                if ty.matches(reference.lock().unwrap().get(key).unwrap().clone()) {
                    opts.push(*key);
                }
            }
            opts.push(*key);
            // less and less likely to make a new one?
            let mod_key = opts.choose(r).unwrap().clone();

            let com = if keys.len() == 0 || mod_key == *key {
                VarOp::Set
            } else {
                ty.ops(reference.clone()).unwrap().choose(r).unwrap().clone()
            };

            if let VarOp::Set = com {
                if mod_key == *key {
                    keys.push(mod_key);
                    let mut lock = reference.lock().unwrap();
                    if let Type::Scope(a, _) = ty {
                        let ity = lock.get(&a).unwrap().clone();
                        lock.insert(mod_key, ity);
                    } else {
                        lock.insert(mod_key, ty.clone());
                    }
                    *key += 1;
                }
                if !ty.is_scope() && r.gen_bool(0.5) {
                    ty = match ty {
                        Type::Float(_) => Type::Float(Default::default()),
                        Type::Int(_) => Type::Int(Default::default()),
                        Type::Bool(_) => Type::Bool(Default::default()),
                        _ => unreachable!()
                    };
                }
            }

            Some(Box::new(Var(com, mod_key, ty)))
        }
        1 => {
            // if
            if keys.len() == 0 {
                if let Outer::Block = outer {
                    return None;
                }
                block!();
            }
            let mut comps: Vec<Compare> = Vec::new();
            let mut go = true;
            while go {
                let t1k = keys.choose(r).unwrap().clone();
                let t1 = Type::Scope(t1k.clone(), vars.clone());
                let t2 = gen_forced_type(r, t1.clone(), vars.clone(), keys, reference.clone());
                let comp = Compare(
                    t1,
                    t2.clone(),
                    Comparison::iter().choose(r).unwrap(),
                    Connector::iter().choose(r).unwrap()
                );
                let contains = {
                    let lock = reference.lock().unwrap();
                    let t1t = lock.get(&t1k).unwrap();
                    let t2t = if let Type::Scope(a,_) = t2 {
                        lock.get(&a).unwrap()
                    } else {
                        &t2
                    };
                    comps.iter().any(|v| {
                        let v0 = if let Type::Scope(a, _) = &v.0 {
                            lock.get(a).unwrap()
                        } else {
                            &v.0
                        };
                        let v1 = if let Type::Scope(a, _) = &v.1 {
                            lock.get(a).unwrap()
                        } else {
                            &v.1
                        };
                        (v0 == t1t || v1 == t1t)
                            && (v0 == t2t || v1 == t2t)
                            && v.2 == comp.2
                    })
                };
                if !contains {
                    comps.push(comp);
                }
                let p = r.gen();
                go = r.gen_bool(p);
            }

            let mut if_keys = keys.clone();
            if let Some(branchif) = gen_runnable(r, reference.clone(), vars.clone(), &mut if_keys, key, Outer::If, outer, recur+1) {
                let branchelse = if r.gen_bool(0.5) {
                    let mut else_keys = keys.clone();
                    gen_runnable(r, reference.clone(), vars, &mut else_keys, key, Outer::If, outer, recur+1)
                } else {
                    None
                };

                Some(Box::new(If(comps, branchif, branchelse)))
            } else {
                None
            }
        }
        2 => {
            // for
            if outer.is_for() || outer_parent.is_for() {
                if r.gen_bool(0.8) {
                    return None;
                }
            }
            let mut our_keys = keys.clone();
            if let Some(inner) = gen_runnable(r, reference.clone(), vars.clone(), &mut our_keys, key, Outer::For, outer, recur+1) {
                Some(Box::new(Loop(r.gen_range(0..=100), inner)))
            } else {
                None
            }
        }
        3 => {
            // block
            block!();
        }
        _ => unreachable!()
    }
}

fn gen_code(vars: Rc<Mutex<HashMap<u16, Type>>>,) -> Main {
    let mut scope_keys: Vec<u16> = {
        let lock = vars.lock().unwrap();
        lock.keys().copied().collect()
    };
    let len = scope_keys.len() as u16;
    let mut r = rand::thread_rng();
    let mut main = Vec::new();
    let mut key = len;
    let reference = Rc::new(Mutex::new({
        let lock = vars.lock().unwrap();
        lock.iter().map(|(k,v)| (*k, v.clone())).collect()
    }));
    let mut go = true;
    while go {
        if let Some(runnable) = gen_runnable(&mut r, reference.clone(), vars.clone(), &mut scope_keys, &mut key, Outer::Block, Outer::Block, 0) {
            main.push(runnable);
            let p = r.gen();
            go = r.gen_bool(p);
        }
    }

    Main(Block(main), vars)
}

fn main() {
    let mut map = HashMap::new();
    map.insert(0, Type::Int(10));
    map.insert(1, Type::Int(15));
    let vars = Rc::new(Mutex::new(map));
    let code = gen_code(vars.clone());
    println!("{}", code);
    code.run();
    let lock = vars.lock().unwrap();
    println!("Final Vars: {:?}", lock);
}
