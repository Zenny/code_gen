use std::collections::HashMap;
use crate::wraptype::{Type, VarOp};
use std::sync::Mutex;
use std::rc::Rc;
use strum::EnumIter;
use std::fmt;

pub trait RunDisplay: Runnable + fmt::Display {}

pub trait Runnable {
    fn run(&self, scope: Rc<Mutex<HashMap<u16, Type>>>) -> Type;
}

pub struct Main(pub Block, pub Rc<Mutex<HashMap<u16, Type>>>);

impl Main {
    pub fn run(&self) -> Type {
        self.0.run(self.1.clone())
    }
}

impl fmt::Display for Main {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let args = {
            let mut s = String::new();
            for i in 0..self.1.lock().unwrap().len() {
                if i > 0 {
                    s += ", ";
                }
                s += &format!("a{}", i);
            }
            s
        };
        write!(f, "fn main({}) \n{}\n", args, self.0)
    }
}

pub struct Block(pub Vec<Box<dyn RunDisplay>>);

impl RunDisplay for Block {}

impl Runnable for Block {
    fn run(&self, scope: Rc<Mutex<HashMap<u16, Type>>>) -> Type {
        for runnable in &self.0 {
            let result = runnable.run(scope.clone());
            if !result.is_none() {
                return result;
            }
        }
        Type::None
    }
}


impl fmt::Display for Block {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(width) = f.width() {
            // If we received a width, we use it
            write!(f, "{:width$}{{\n{}\n{:width$}}}",
                   "",
                   self.0.iter().map(|v| format!("{:nw$}", v, nw=width+4)).collect::<Vec<_>>().join("\n"),
                   "",
                   width=width)
        } else {
            // Otherwise we do nothing special
            write!(f, "{{\n{}\n}}", self.0.iter().map(|v| format!("{:4}", v)).collect::<Vec<_>>().join("\n"))
        }

    }
}

#[derive(Clone, EnumIter, PartialEq)]
pub enum Connector {
    And,
    Or,
}

impl fmt::Display for Connector {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Connector::And => write!(f, "&&"),
            Connector::Or => write!(f, "||"),
        }
    }
}

#[derive(EnumIter, PartialEq)]
pub enum Comparison {
    Eq,
    Neq,
    Gt,
    Lt,
    Le,
    Ge,
}

impl fmt::Display for Comparison {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Comparison::Eq => write!(f, "=="),
            Comparison::Neq => write!(f, "!="),
            Comparison::Gt => write!(f, ">"),
            Comparison::Lt => write!(f, "<"),
            Comparison::Le => write!(f, "<="),
            Comparison::Ge => write!(f, ">="),
        }
    }
}

/// Compare 2 values, must be comparable and the same type.
/// It uses the connector if another comparison comes after it
#[derive(PartialEq)]
pub struct Compare(pub Type, pub Type, pub Comparison, pub Connector);

impl Compare {
    pub fn run(&self) -> bool {
        match self.2 {
            Comparison::Eq => self.0 == self.1,
            Comparison::Neq => self.0 != self.1,
            Comparison::Gt => self.0 > self.1,
            Comparison::Lt => self.0 < self.1,
            Comparison::Le => self.0 <= self.1,
            Comparison::Ge => self.0 >= self.1,
        }
    }
}

impl fmt::Display for Compare {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {} {}", self.0, self.2, self.1)
    }
}

/// Iterations, block
pub struct Loop(pub u8, pub Box<dyn RunDisplay>);

impl RunDisplay for Loop {}

impl Runnable for Loop {
    fn run(&self, scope: Rc<Mutex<HashMap<u16, Type>>>) -> Type {
        for _ in 0..self.0 {
            self.1.run(scope.clone());
        }
        Type::None
    }
}

impl fmt::Display for Loop {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(width) = f.width() {
            write!(f, "{:width$}for _ in 0..{} {{\n{:nw$}\n{:width$}}}", "", self.0, self.1, "", nw=width+4, width=width)
        } else {
            write!(f, "for _ in 0..{} {{\n{:4}\n}}", self.0, self.1)
        }
    }
}

/// Comparison code, A block to run on if, a block to run on else
pub struct If(pub Vec<Compare>, pub Box<dyn RunDisplay>, pub Option<Box<dyn RunDisplay>>);

impl RunDisplay for If {}

impl Runnable for If
{
    fn run(&self, scope: Rc<Mutex<HashMap<u16, Type>>>) -> Type {
        let mut res = vec![vec![]];
        for comp in self.0.iter() {
            if let Connector::Or = comp.3 {
                let l = res.last_mut().unwrap();
                l.push(comp.run());
                res.push(vec![]);
            } else if let Connector::And = comp.3 {
                let l = res.last_mut().unwrap();
                l.push(comp.run());
            }
        }
        if res.iter().any(|r| r.iter().all(|v| *v)) {
            self.1.run(scope)
        } else if let Some(runnable) = &self.2 {
            runnable.run(scope)
        } else {
            Type::None
        }
    }
}

impl fmt::Display for If {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut comps = String::new();
        for (i, comp) in self.0.iter().enumerate() {
            if i > 0 {
                comps += &format!(" {} ", comp.3);
            }

            comps += &format!("{}", comp);
        }
        if let Some(width) = f.width() {
            write!(f, "{:width$}if {} {{\n{:nw$}\n{:width$}}}{}", "", comps, self.1, "",
                   if let Some(el) = &self.2 {
                       format!(" else {{\n{:nw$}\n{:width$}}}", el, "", nw=width+4, width=width)
                   } else {
                       String::new()
                   }, nw=width+4, width=width)
        } else {
            write!(f, "if {} {{\n{:4}\n}}{}", comps, self.1,
                   if let Some(el) = &self.2 {
                       format!(" else {{\n{:4}\n}}", el)
                   } else {
                       String::new()
                   })
        }
    }
}

/// Op, ID, Value,
pub struct Var(pub VarOp, pub u16, pub Type);

impl RunDisplay for Var {}

impl Runnable for Var {
    fn run(&self, scope: Rc<Mutex<HashMap<u16, Type>>>) -> Type {
        macro_rules! math {
            ($op: tt) => {
                {
                    // guarantee: knows it exists
                    let a = scope.lock().unwrap().remove(&self.1).unwrap();
                    let res = a $op self.2.clone();
                    scope.lock().unwrap().insert(self.1, res);
                }
            }
        }
        match self.0 {
            VarOp::Set => {
                match self.2 {
                    Type::Scope(a, _) => {
                        let mut lock = scope.lock().unwrap();
                        let val = lock.get(&a).unwrap().clone();
                        lock.insert(self.1, val);
                    }
                    _ => {
                        scope.lock().unwrap().insert(self.1, self.2.clone());
                    }
                }
            }
            VarOp::Add => math!(+),
            VarOp::Sub => math!(-),
            VarOp::Mul => math!(*),
            VarOp::Div => math!(/),
            VarOp::Mod => math!(%),
            VarOp::And => math!(&),
            VarOp::Or => math!(|),
            VarOp::Xor => math!(^),
            VarOp::Not => {
                let a = scope.lock().unwrap().remove(&self.1).unwrap();
                let res = !a;
                scope.lock().unwrap().insert(self.1, res);
            }
        }
        Type::None
    }
}

impl fmt::Display for Var {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(width) = f.width() {
            write!(f, "{:width$}{}a{} {};", "", if let VarOp::Set = self.0 {
                "let "
            } else {
                ""
            }, self.1, &match self.0 {
                VarOp::Set => format!("= {}", self.2),
                VarOp::Add => format!("+= {}", self.2),
                VarOp::Sub => format!("-= {}", self.2),
                VarOp::Mul => format!("*= {}", self.2),
                VarOp::Div => format!("/= {}", self.2),
                VarOp::Mod => format!("%= {}", self.2),
                VarOp::And => format!("&= {}", self.2),
                VarOp::Or => format!("|= {}", self.2),
                VarOp::Xor => format!("^= {}", self.2),
                VarOp::Not => format!("= !a{}", self.1),
            }, width=width)
        } else {
            write!(f, "{}a{} {};", if let VarOp::Set = self.0 {
                "let "
            } else {
                ""
            }, self.1, &match self.0 {
                VarOp::Set => format!("= {}", self.2),
                VarOp::Add => format!("+= {}", self.2),
                VarOp::Sub => format!("-= {}", self.2),
                VarOp::Mul => format!("*= {}", self.2),
                VarOp::Div => format!("/= {}", self.2),
                VarOp::Mod => format!("%= {}", self.2),
                VarOp::And => format!("&= {}", self.2),
                VarOp::Or => format!("|= {}", self.2),
                VarOp::Xor => format!("^= {}", self.2),
                VarOp::Not => format!("= !a{}", self.1),
            })
        }
    }
}