use std::cmp::Ordering;
use std::collections::HashMap;
use std::sync::Mutex;
use std::rc::Rc;
use std::ops::*;
use strum::EnumIter;
use std::fmt;

#[derive(Clone, EnumIter)]
pub enum VarOp {
    Set,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    And,
    Or,
    Xor,
    Not,
}

#[derive(Debug, Clone, EnumIter)]
pub enum Type {
    Float(f64),
    Int(i128),
    Bool(bool),
    Scope(u16, Rc<Mutex<HashMap<u16, Self>>>),
    None,
}

impl Type {
    pub fn ops(&self, reference: Rc<Mutex<HashMap<u16, Self>>>) -> Option<Vec<VarOp>> {
        match self {
            Type::Int(_) =>
                Some(
                    vec![
                        VarOp::Set,
                        VarOp::Add,
                        VarOp::Sub,
                        VarOp::Mul,
                        VarOp::Div,
                        VarOp::Mod,
                        VarOp::And,
                        VarOp::Or,
                        VarOp::Xor,
                        VarOp::Not,
                    ]
                ),
            Type::Bool(_) => Some(vec![VarOp::Set, VarOp::Not]),
            Type::Scope(a, _) => {
                let ty = {
                    let lock = reference.lock().unwrap();
                    lock.get(a).expect(&format!("Couldn't get {} {:?}", a, lock)).clone()
                };
                ty.ops(reference)
            }
            Type::Float(_) => Some(
                vec![
                    VarOp::Set,
                    VarOp::Add,
                    VarOp::Sub,
                    VarOp::Mul,
                    VarOp::Div,
                    VarOp::Mod,
                ]
            ),
            Type::None => None,
        }
    }

    pub fn matches(&self, other: Self) -> bool {
        match self {
            Type::Float(_) => if let Type::Float(_) = other {
                true
            } else {
                false
            },
            Type::Int(_) =>  if let Type::Int(_) = other {
                true
            } else {
                false
            },
            Type::Bool(_) =>  if let Type::Bool(_) = other {
                true
            } else {
                false
            },
            Type::Scope(_, _) =>  if let Type::Scope(_,_) = other {
                true
            } else {
                false
            },
            Type::None =>  if let Type::None = other {
                true
            } else {
                false
            },
        }
    }

    pub fn is_none(&self) -> bool {
        if let Self::None = self {
            true
        } else {
            false
        }
    }

    pub fn is_scope(&self) -> bool {
        if let Self::Scope(_,_) = self {
            true
        } else {
            false
        }
    }

    pub fn bool(&self) -> Option<bool> {
        if let Self::Bool(v) = self {
            Some(*v)
        } else {
            None
        }
    }

    pub fn scope(&self) -> Option<Self> {
        if let Self::Scope(v, h) = self {
            Some(h.lock().unwrap().get(v).unwrap().to_owned())
        } else {
            None
        }
    }
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Type::Float(a) => write!(f, "{}", a),
            Type::Int(a) => write!(f, "{}", a),
            Type::Bool(a) => write!(f, "{}", a),
            Type::Scope(a, _) => write!(f, "a{}", a),
            Type::None => write!(f, "None"),
        }
    }
}

// TODO: Checked math fns instead? Release build is supposed to allow overflow (Which we want)
macro_rules! math {
    ($m: ident, $f: ident, $op: tt) => {
        impl $m for Type {
            type Output = Type;

            fn $f(self, rhs: Self) -> Self::Output {
                match self {
                    Type::Float(a) => match rhs {
                        Type::Float(b) => return Type::Float(a $op b),
                        Type::Scope(b, ref h) => {
                            let lock = h.lock().unwrap();
                            if let Type::Float(b) = lock.get(&b).unwrap() {
                                return Type::Float(a $op b);
                            }
                        },
                        _ => {}
                    }
                    Type::Int(a) => match rhs {
                        Type::Int(b) => return Type::Int(a $op b),
                        Type::Scope(b, ref h) => {
                            let lock = h.lock().unwrap();
                            if let Type::Int(b) = lock.get(&b).unwrap() {
                                return Type::Int(a $op b);
                            }
                        }
                        _ => {}
                    }
                    Type::Scope(a, ref h) => {
                        let lock = h.lock().unwrap();
                        match lock.get(&a).unwrap() {
                            Type::Float(a) => {
                                if let Type::Float(b) = rhs {
                                    return Type::Float(a $op b);
                                }
                            }
                            Type::Int(a) => {
                                if let Type::Int(b) = rhs {
                                    return Type::Int(a $op b);
                                }
                            }
                            _ => {}
                        }
                    }
                    _ => {}
                }
                panic!("{} {:?} {:?}", stringify!($f), self, rhs);
            }
        }
    }
}

math!(Add, add, +);
math!(Sub, sub, -);
math!(Mul, mul, *);
math!(Div, div, /);
math!(Rem, rem, %);

macro_rules! bitwise {
    ($m: ident, $f: ident, $op: tt) => {
        impl $m for Type {
            type Output = Type;

            fn $f(self, rhs: Self) -> Self::Output {
                match self {
                    Type::Int(a) => match rhs {
                        Type::Int(b) => return Type::Int(a $op b),
                        Type::Scope(b, ref h) => {
                            let lock = h.lock().unwrap();
                            if let Type::Int(b) = lock.get(&b).unwrap() {
                                return Type::Int(a $op b);
                            }
                        }
                        _ => {}
                    }
                    _ => {}
                }
                panic!("{} {:?} {:?}", stringify!($f), self, rhs);
            }
        }
    }
}

bitwise!(BitAnd, bitand, &);
bitwise!(BitOr, bitor, |);
bitwise!(BitXor, bitxor, ^);

impl Not for Type {
    type Output = Type;

    fn not(self) -> Self::Output {
        match self {
            Type::Int(a) => return Type::Int(!a),
            Type::Bool(a) => return Type::Bool(!a),
            Type::Scope(a, h) => {
                let lock = h.lock().unwrap();
                match lock.get(&a).unwrap() {
                    Type::Int(a) => return Type::Int(!*a),
                    Type::Bool(a) => return Type::Bool(!*a),
                    _ => {}
                }
            }
            _ => {}
        }
        unreachable!()
    }
}

impl PartialEq for Type {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Type::Bool(a) => match other {
                Type::Bool(b) => return a == b,
                _ => {}
            }
            Type::Scope(a, h) => {
                let lock = h.lock().unwrap();
                let inner = lock.get(a).unwrap();
                match other {
                    Type::Scope(b, _) => {
                        return inner == lock.get(b).unwrap();
                    }
                    Type::Float(b) => {
                        if let Type::Float(a) = inner {
                            return a == b;
                        }
                    }
                    Type::Int(b) => {
                        if let Type::Int(a) = inner {
                            return a == b;
                        }
                    }
                    Type::Bool(b) => {
                        if let Type::Bool(a) = inner {
                            return a == b;
                        }
                    }
                    _ => {}
                }
            }
            Type::Int(a) => match other {
                Type::Int(b) => return a == b,
                _ => {}
            }
            Type::Float(a) => match other {
                Type::Float(b) => return a == b,
                _ => {}
            }
            Type::None => match other {
                Type::None => return true,
                _ => {}
            },
        }
        false
    }

    fn ne(&self, other: &Self) -> bool {
        !self.eq(other)
    }
}

impl PartialOrd for Type {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self {
            Type::Bool(a) => match other {
                    Type::Bool(b) => return a.partial_cmp(b),
                    _ => {}
            }
            Type::Scope(a, h) => {
                let lock = h.lock().unwrap();
                let inner = lock.get(a).unwrap();
                match other {
                    Type::Scope(b, _) => {
                        return inner.partial_cmp(lock.get(b).unwrap());
                    }
                    Type::Float(b) => {
                        if let Type::Float(a) = inner {
                            return a.partial_cmp(b);
                        }
                    }
                    Type::Int(b) => {
                        if let Type::Int(a) = inner {
                            return a.partial_cmp(b);
                        }
                    }
                    Type::Bool(b) => {
                        if let Type::Bool(a) = inner {
                            return a.partial_cmp(b);
                        }
                    }
                    _ => {}
                }
            }
            Type::Int(a) => match other {
                Type::Int(b) => return a.partial_cmp(b),
                _ => {}
            }
            Type::Float(a) => match other {
                Type::Float(b) => return a.partial_cmp(b),
                _ => {}
            }
            _ => {}
        }
        None
    }
}
