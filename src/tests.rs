use crate::wraptype::Type;
use std::rc::Rc;
use std::sync::Mutex;
use std::collections::HashMap;
use crate::codes::{Comparison, Compare, Connector};

#[test]
fn compare() {
    //let a2 = -7482.083145622575;
    //a2 < -3574.4796606378404
    let c = Rc::new(Mutex::new(HashMap::new()));
    {
        let mut lock = c.lock().unwrap();
        lock.insert(0, Type::Float(-7482.083145622575));
    }
    let t1 = Type::Scope(0, c);
    let t2 = Type::Float(-3574.4796606378404);
    let comp = Compare(t1, t2, Comparison::Lt, Connector::Or);
    assert!(comp.run(), true);
}